﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class EditPeopleLogic
    {
        private const string Name = "name";
        private const string BirthDay = "birth";
        private const string DeathDay = "death";
        private const string ParentsID = "parents";

        static string ModifiedString(Person person, string _new, string modification, string name, string path)
        {
            StreamReader sr = new StreamReader(path);
            string file = File.ReadAllText(path);
            while (!sr.EndOfStream)
            {
                string partFiles = sr.ReadLine();
                string editPartFiles = null;
                int i = 0;
                string str = "";
                while (partFiles[i] != ';')
                {
                    str += partFiles[i];
                    i++;
                }
                if (int.Parse(str) == person.id)
                {
                    if (person.deathday == null && name == "deathday")
                        editPartFiles = partFiles.Replace(Convert.ToString(person.birthday.ToShortDateString()), (Convert.ToString(person.birthday.ToShortDateString()) + ';' + _new));
                    else if (name == "parent1" && person.parentId1 == null && person.deathday != null)
                        editPartFiles = partFiles.Replace(Convert.ToString(ParseDateTime.ParseDate(Convert.ToString(person.deathday)).ToShortDateString()), (Convert.ToString(ParseDateTime.ParseDate(Convert.ToString(person.deathday)).ToShortDateString()) + ';' + _new));
                    else if (name == "parent1" && person.parentId1 == null && person.deathday == default)
                        editPartFiles = partFiles.Replace(Convert.ToString(person.birthday.ToShortDateString()), (Convert.ToString(person.birthday.ToShortDateString()) + ';' + null + ';' + _new));
                    else if (name == "parent2" && person.parentId2 == null)
                        editPartFiles = partFiles.Replace(Convert.ToString(person.parentId1), (Convert.ToString(person.parentId1) + ';' + _new));
                    else
                        editPartFiles = partFiles.Replace(modification, _new);
                    if (editPartFiles[editPartFiles.Length - 1] != ';')
                        editPartFiles += ';';
                    file = file.Replace(partFiles, editPartFiles);
                    break;
                }

            }
            sr.Close();
            return file;
        }

        static Person EditName(Person person, string path)
        {
            ConsoleHelper.ClearScreen();
            person.DisplayInfo();
            Console.WriteLine();
            Console.WriteLine("Введите новое имя: ");
            string newName = Console.ReadLine();
            person.name = newName;
            if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                File.WriteAllText(path, ModifiedString(person, newName, person.name, "name", path));
            return person;
        }



        static Person EditBirthday(Person person, string path)
        {
            ConsoleHelper.ClearScreen();
            person.DisplayInfo();
            Console.WriteLine();
            string newDate = NewBirthOrDeathDay.ReadBirthday(person.name);
            if (person.parentId1 != null || person.parentId2 != null)
                if (person.parentId1 != null)
                    while (!CheckingForCorrectness.BirthDayChild(person.parentId1, person.birthday)) ;
                else
                    while (!CheckingForCorrectness.BirthDayChild(person.parentId2, person.birthday)) ;
            if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                File.WriteAllText(path, ModifiedString(person, newDate, Convert.ToString(person.birthday.ToShortDateString()), "birthday", path));
            person.birthday = ParseDateTime.ParseDate(newDate);
            return person;
        }

        static Person EditDeathDay(Person person, string path)
        {
            ConsoleHelper.ClearScreen();
            person.DisplayInfo();
            Console.WriteLine();
            string newDate = NewBirthOrDeathDay.ReadDeathDay(person.name, Convert.ToString(person.birthday));
            if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                File.WriteAllText(path, ModifiedString(person, newDate, Convert.ToString(ParseDateTime.ParseDate(Convert.ToString(person.deathday)).ToShortDateString()), "deathday", path));
            person.deathday = ParseDateTime.ParseDate(newDate);
            return person;
        }

        static Person EditParents(Person person, List<Person> people, string path)
        {
            ConsoleHelper.ClearScreen();
            person.DisplayInfo();
            Console.WriteLine();
            Console.WriteLine("Введите количество родителей: ");
            int volParents = int.Parse(Console.ReadLine());
            while (volParents > 2 || volParents < 0)
            {
                Console.WriteLine("Ошибка! Введите корректное количество родителей(0-2)");
                volParents = int.Parse(Console.ReadLine());
            }
            if (volParents == 0)
            {
                if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                {
                    if (person.parentId1 != null)
                        File.WriteAllText(path, ModifiedString(person, "", Convert.ToString(person.parentId1), "parent1", path));
                    if (person.parentId2 != null)
                        File.WriteAllText(path, ModifiedString(person, "", Convert.ToString(person.parentId2), "parent2", path));
                }
                person.parentId1 = null;
                person.parentId2 = null;
            }
            else if (volParents == 1)
            {
                Console.WriteLine("Выберите родителя: ");
                Person parent = FindPeopleLogic.FindPeople(people);
                if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                {
                    File.WriteAllText(path, ModifiedString(person, Convert.ToString(parent.id), Convert.ToString(person.parentId1), "parent1", path));
                    if (person.parentId2 != null)
                        File.WriteAllText(path, ModifiedString(person, "", Convert.ToString(person.parentId2), "parent2", path));
                }
                person.parentId1 = parent.id;
                person.parentId2 = null;
            }
            else
            {
                Console.WriteLine("Выберите первого родителя: ");
                Person parent = FindPeopleLogic.FindPeople(people);
                if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                    File.WriteAllText(path, ModifiedString(person, Convert.ToString(parent.id), Convert.ToString(person.parentId1), "parent1", path));
                person.parentId1 = parent.id;
                Console.WriteLine("Выберите второго родителя: ");
                for (int i = 0; i < people.Count; i++)
                    if (people[i].id == person.id)
                    {
                        person = people[i];
                        break;
                    }
                parent = FindPeopleLogic.FindPeople(people);
                if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                    File.WriteAllText(path, ModifiedString(person, Convert.ToString(parent.id), Convert.ToString(person.parentId2), "parent2", path));
                person.parentId2 = parent.id;
            }
            return person;
        }

        public static List<Person> EditPerson(List<Person> people)
        {
            string path = WorkingWithTheExtension.Start("people");
            Console.WriteLine("Выберите человека, данные которого хотите изменить: ");
            Person person = FindPeopleLogic.FindPeople(people);
            ConsoleHelper.ClearScreen();
            person.DisplayInfo();
            Console.WriteLine("Что вы хотите изменить?");
            Console.CursorVisible = false;
            List<MenuItem> menu = new List<MenuItem>
            {
                new MenuItem {Id = Name, Text = "Имя", IsSelected = true},
                new MenuItem {Id = BirthDay, Text = "Дату рождения" },
                new MenuItem {Id = DeathDay, Text = "Дату смерти" },
                new MenuItem {Id = ParentsID, Text = "ID родителей" },
                new MenuItem {Id = "exit", Text = "Выход" }
            };

            bool exit = false;
            do
            {
                Program.DrawMenu(menu);

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        Program.MenuSelectNext(menu);
                        break;
                    case ConsoleKey.UpArrow:
                        Program.MenuSelectPrev(menu);
                        break;
                    case ConsoleKey.Enter:
                        var selectedItem = menu.First(x => x.IsSelected);
                        person = Perform(selectedItem.Id, person, people, path);
                        for (int i = 0; i < people.Count; i++)
                            if (people[i].id == person.id)
                                people[i] = person;

                        Console.WriteLine("Хотите продолжить? y/n");
                        string answer = Console.ReadLine();
                        exit = answer == "n" || answer == "no";

                        return people;
                }
            } while (!exit);
            JSON.WritePeopleJSON(people, path);
            return people;
        }

        static Person Perform(string commandId, Person person, List<Person> people, string path)
        {
            ConsoleHelper.ClearScreen();
            switch (commandId)
            {
                case Name:
                    person = EditName(person, path);
                    break;
                case BirthDay:
                    person = EditBirthday(person, path);
                    break;
                case DeathDay:
                    person = EditDeathDay(person, path);
                    break;
                case ParentsID:
                    person = EditParents(person, people, path);
                    break;
            }
            return person;
        }
    }
}
