﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class NewBirthOrDeathDay
    {
        public static string ReadBirthday(string name)
        {
            Console.WriteLine("Введите дату рождения: ");
            string birthDay = Console.ReadLine();
            while (!CheckingForCorrectness.DateCorrectness(birthDay))
            {
                ConsoleHelper.ClearScreen();
                Console.WriteLine($"Имя: {name}");
                Console.WriteLine("Ошибка! Введите дату рождения: ");
                birthDay = Console.ReadLine();
            }
            return birthDay;
        }

        public static string ReadDeathDay(string name, string birthDay)
        {
            Console.WriteLine("Если человек жив нажмите Enter, иначе нажмите любую букву: ");
            ConsoleKeyInfo keyInfo = Console.ReadKey(true);
            string deadDay = null;
            if (keyInfo.Key == ConsoleKey.Enter)
                deadDay = null; 
            else
            {
                Console.WriteLine("Введите дату смерти: ");
                deadDay = Console.ReadLine();
                while (!CheckingForCorrectness.DateCorrectness(deadDay))
                {
                    ConsoleHelper.ClearScreen();
                    Console.WriteLine($"Имя: {name}");
                    Console.WriteLine($"Дата рождения: {birthDay}");
                    Console.WriteLine("Ошибка! Введите корректную дату смерти: ");
                    deadDay = Console.ReadLine();
                }
                while (ParseDateTime.ParseDate(deadDay) < ParseDateTime.ParseDate(birthDay))
                {
                    ConsoleHelper.ClearScreen();
                    Console.WriteLine($"Имя: {name}");
                    Console.WriteLine("Ошибка! Нельзя умереть раньше, чем родиться! ");
                    Console.WriteLine($"Дата рождения: {ParseDateTime.ParseDate(birthDay)}");
                    Console.WriteLine("Введите дату смерти: ");
                    deadDay = ReadDeathDay(name, birthDay);
                }
            }

            return deadDay;
        }
    }
}
