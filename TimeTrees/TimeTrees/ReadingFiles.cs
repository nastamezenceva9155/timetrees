﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TimeTrees
{
    class ReadingFiles
    {
        static List<Person> ReadPersonJSON(string pathPeopleJson)
        {
            var jsonPersonString = File.ReadAllText(pathPeopleJson);
            return JsonConvert.DeserializeObject<List<Person>>(jsonPersonString.Replace(';', ' '));
        }

        static List<TimelineEvent> ReadTimelineJSON(string pathTimeLineJson)
        {
            var jsonTimelineString = File.ReadAllText(pathTimeLineJson);
            return JsonConvert.DeserializeObject<List<TimelineEvent>>(jsonTimelineString.Replace(';', ' '));
        }

        static List<Person> ReadPeopleCSV(string pathPeopleCsv)
        {
            string[][] people = File.ReadLines(pathPeopleCsv)
                                      .Select(s => s.Split(';').ToArray())
                                      .ToArray();
            List<Person> people2 = TransferFromArrayToStruct.PeopleToPerson(people);
            return people2;
        }

        static List<TimelineEvent> ReadTimelineCSV(string pathTimeLine)
        {
            string[][] timeLine = File.ReadLines(pathTimeLine)
                                      .Select(s => s.Split(';').ToArray())
                                      .ToArray();
            int[][] ParseArray = timeLine.Select(i => ArrayParse(i[0])).ToArray();
            List<TimelineEvent> timeLine2 = TransferFromArrayToStruct.TimeLineToEvent(timeLine);
            return timeLine2;
        }

        public static List<Person> ReadingPeople()
        {
            string pathPeopleCsv = @"..\..\..\..\people.csv";
            string pathPeopleJson = @"..\..\..\..\people.json";
            DateTime dateChandeCsv = File.GetLastWriteTime(pathPeopleCsv);
            DateTime dateChandeJson = File.GetLastWriteTime(pathPeopleJson);
            List<Person> people;
            if (dateChandeCsv > dateChandeJson)
                people = ReadPeopleCSV(pathPeopleCsv);
            else
                people = ReadPersonJSON(pathPeopleJson);
            return people;
        }

        static int[] ArrayParse(object s)
        {
            int[] array = new int[3] { 0, 0, 0 };
            string s1 = Convert.ToString(s);
            string[] splitedstr = s1.Split('.').ToArray();
            if (splitedstr.Length == 1)
                splitedstr = s1.Split('-').ToArray();

            for (int i = splitedstr.Length - 1; i >= 0; i--)
            {
                array[2 - i] = int.Parse(splitedstr[i]);
            }

            return array;
        }

        public static List<TimelineEvent> ReadingEvents()
        {
            string pathTimeLineCsv = @"..\..\..\..\timeline.csv";
            string pathTimeLineJson = @"..\..\..\..\timeline.json";
            DateTime dateChandeCsv = File.GetLastWriteTime(pathTimeLineCsv);
            DateTime dateChandeJson = File.GetLastWriteTime(pathTimeLineJson);
            List<TimelineEvent> timeline;
            if (dateChandeCsv > dateChandeJson)
                timeline = ReadTimelineCSV(pathTimeLineCsv);
            else
                timeline = ReadTimelineJSON(pathTimeLineJson);
            return timeline;
        }
    }
}
