﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class ParseDateTime
    {
        public static DateTime ParseDate(string line)
        {
            DateTime date;
            if (!DateTime.TryParseExact(line, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(line, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(line, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        if (!DateTime.TryParseExact(line, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        {
                            if (!DateTime.TryParseExact(line, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                            {
                                date = default;
                            }
                        }
                    }
                }
            }
            return date;
        }
    }
}
