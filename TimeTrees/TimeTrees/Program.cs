﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.Json;


namespace TimeTrees
{
    class Program
    {
        private const string DeltaProgramId = "delta";
        private const string LeapYearProgramId = "leap";
        private const string FindProgramId = "find";
        private const string ViewProgramId = "view";
        private const string AddProgramId = "add";
        private const string EditProgramId = "edit";
        private const string EditPeople = "EditPeople";
        private const string OutputPeople = "outPeople";
        private const string OutputEvents = "outEvents";

        public static void DrawMenu(List<MenuItem> menu)
        {
            ConsoleHelper.ClearScreen();
            foreach (MenuItem menuItem in menu)
            {
                Console.BackgroundColor = menuItem.IsSelected
                    ? ConsoleColor.DarkRed
                    : ConsoleColor.Black;

                Console.WriteLine(menuItem.Text);
            }

            Console.BackgroundColor = ConsoleColor.Black;
        }
        public static void MenuSelectNext(List<MenuItem> menu)
        {
            var selectedItem = menu.First(x => x.IsSelected);
            int selectedIndex = menu.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedIndex = selectedIndex == menu.Count - 1
                ? 0
                : ++selectedIndex;

            menu[selectedIndex].IsSelected = true;
        }

        public static void MenuSelectPrev(List<MenuItem> menu)
        {
            var selectedItem = menu.First(x => x.IsSelected);
            int selectedIndex = menu.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedIndex = selectedIndex == 0
                ? menu.Count - 1
                : --selectedIndex;

            menu[selectedIndex].IsSelected = true;
        }

        public static void Execute(string commandId, List<Person> people, List<TimelineEvent> timeline)
        {
            ConsoleHelper.ClearScreen();
            switch (commandId)
            {
               case DeltaProgramId:
                    SearchDeltaDatesLogic.ExecuteDeltaProgram(timeline);
                    break;
                case LeapYearProgramId:
                    LeapYearLogic.ExecuteLeapYearProgram(people);
                    break;
                case FindProgramId:
                    FindPeopleLogic.FindPeople(people);
                    break;
                case AddProgramId:
                    AddProgramLogic.ExecuteAddProgram(people, timeline);
                    break;
                case AddProgramLogic.AddEvent:
                    AddProgramLogic.ExecuteAddEvent(people, timeline);
                    break;
                case AddProgramLogic.AddPeople:
                    people = AddProgramLogic.ExecuteAddPeople(people);
                    break;
                case EditPeople:
                    people = EditPeopleLogic.EditPerson(people);
                    break;
                case OutputPeople:
                    OutputInfo.DisplayPeople();
                    break;
                case OutputEvents:
                    OutputInfo.DisplayEvents();
                    break;

                case "exit":
                    break;
            }
        }


        static void Main()
        {
            List<Person> people = ReadingFiles.ReadingPeople();
            List<TimelineEvent> timeline = ReadingFiles.ReadingEvents();
            Console.CursorVisible = false;
            List<MenuItem> menu = new List<MenuItem>
            {
                new MenuItem {Id = DeltaProgramId, Text = "Найти дельту дат между событиями", IsSelected = true},
                new MenuItem {Id = LeapYearProgramId, Text = "Найти людей, родившихся в високосный год" },
                new MenuItem {Id = FindProgramId, Text = "Пример поиска людей" },
                new MenuItem {Id = AddProgramId, Text = "Добавить данные" },
                new MenuItem {Id = EditPeople, Text = "Изменить данные человека" },
                new MenuItem {Id = OutputPeople, Text = "Вывести на экран всех людей" },
                new MenuItem {Id = OutputEvents, Text = "Вывести на экран все события" },
                new MenuItem {Id = "exit", Text = "Выход" }
            };

            bool exit = false;
            do
            {
                DrawMenu(menu);

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        MenuSelectNext(menu);
                        break;
                    case ConsoleKey.UpArrow:
                        MenuSelectPrev(menu);
                        break;
                    case ConsoleKey.Enter:
                        var selectedItem = menu.First(x => x.IsSelected);
                        Execute(selectedItem.Id, people, timeline);

                        Console.WriteLine("Хотите продолжить? y/n");
                        string answer = Console.ReadLine();
                        exit = answer == "n" || answer == "no";

                        break;
                }
            } while (!exit);
        }
    }
}