﻿using System;
using System.Collections.Generic;

namespace TimeTrees
{
    class OutputInfo
    {
        public static void DisplayPeople()
        {
            List<Person> people = ReadingFiles.ReadingPeople();
            Console.WriteLine();
            for (int i = 0; i < people.Count; i++)
            {
                people[i].DisplayInfo();
                Console.WriteLine();
            }
        }

        public static void DisplayEvents()
        {
            List<TimelineEvent> timeline = ReadingFiles.ReadingEvents();
            for (int i = 0; i < timeline.Count; i++)
            {
                timeline[i].DisplayInfo();
                Console.WriteLine();
            }
        }
    }
}
