﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    public class Person
    {
        public const int IdIndex = 0;
        public const int NameIndex = 1;
        public const int BirthIndex = 2;
        public const int DeathIndex = 3;
        public const int ParentID1 = 4;
        public const int ParentID2 = 5;
        public int id;
        public string name;
        public DateTime birthday;
        public DateTime? deathday;
        public int? parentId1;
        public int? parentId2;
        //public List<Person?> parents = new List<Person>() {null};

        public void DisplayInfo()
        {
            Console.Write($"id: {id}    name: {name}    birthday: {birthday}    deathday: ");
            if (deathday != default)
                Console.Write($"{deathday}  ");
            else
                Console.Write("жив  ");
            Console.Write($"id parents: ");
            if (parentId1 != null || parentId2 != null)
            {
                if (parentId1 == null)
                    Console.Write(parentId2);
                else if (parentId2 == null)
                    Console.Write(parentId1);
                else
                    Console.Write($"{parentId1} {parentId2}");
            }
            else
            {
                Console.WriteLine("нет ");
            }
        }
    }
}
