﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class MenuItem
    {
        public string Id;
        public string Text;
        public bool IsSelected;
    }
}
