﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class CheckingForCorrectness
    {
        public static bool DateCorrectness(string date)
        {
            DateTime dateCheck = ParseDateTime.ParseDate(date);
            if (dateCheck != default)
                return true;
            return false;
        }

        public static bool IdPeopleCorrectness(string id)
        {
            foreach (Person person in ReadingFiles.ReadingPeople())
            {
                if (person.id == Int32.Parse(id))
                    return true;
            }
            return false;
        }
        public static bool BirthDayChild(int? parentId, DateTime child)
        {
            DateTime parent = default;
            List<Person> people = ReadingFiles.ReadingPeople();
            int length = 0;
            foreach (Person person in people)
                length++;
            for (int i = 0; i < length; i++)
                if (people[i].id == parentId)
                {
                    parent = people[i].birthday;
                    break;
                }
            if (parent > child)
                return true;
            else
                return false;
        }

        public static bool ExpansionCorrectness(string expansion)
        {
            if (expansion == "JSON" || expansion == "json" || expansion == "CSV" || expansion == "csv")
                return true;
            else
                return false;
        }

    }
}
