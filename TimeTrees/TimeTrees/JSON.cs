﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class JSON
    {
        public static void WriteTimelineJSON(List<TimelineEvent> timeline, string path)
        {
            string json = JsonConvert.SerializeObject(timeline, Formatting.Indented);
            File.WriteAllText(path, json);
        }

        public static void WritePeopleJSON(List<Person> people, string path)
        {
            string json = JsonConvert.SerializeObject(people, Formatting.Indented);
            File.WriteAllText(path, json);
        }

    }
}
