﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class AddProgramLogic
    {
        public const string AddEvent = "event";
        public const string AddPeople = "people";
        public static void ExecuteAddProgram(List<Person> people, List<TimelineEvent> timeline)
        {
            Console.CursorVisible = false;
            List<MenuItem> menu = new List<MenuItem>
            {
                new MenuItem {Id = AddEvent, Text = "Добавить событие", IsSelected = true},
                new MenuItem {Id = AddPeople, Text = "Добавить людей" },
                new MenuItem {Id = "exit", Text = "Выход" }
            };

            bool exit = false;
            do
            {
                Program.DrawMenu(menu);

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        Program.MenuSelectNext(menu);
                        break;
                    case ConsoleKey.UpArrow:
                        Program.MenuSelectPrev(menu);
                        break;
                    case ConsoleKey.Enter:
                        var selectedItem = menu.First(x => x.IsSelected);
                        Program.Execute(selectedItem.Id, people, timeline);
                        string answer = "no";
                        exit = answer == "n" || answer == "no";
                        break;

                }
            } while (!exit);
        }

        public static void ExecuteAddEvent(List<Person> people, List<TimelineEvent> timeline)
        {
            string path = WorkingWithTheExtension.Start("event");
            Console.WriteLine("Введите дату(год/год и месяц/точная дата): ");
            string date = Console.ReadLine();
            while (!CheckingForCorrectness.DateCorrectness(date))
            {
                ConsoleHelper.ClearScreen();
                Console.WriteLine("Ошибка! Введите дату(год/год и месяц/точная дата): ");
                date = Console.ReadLine();
            }
            Console.WriteLine("Введите описание события: ");
            string eventDescription = Console.ReadLine();
            Console.WriteLine("Введите сколько людей, участвовавших в этом событии: ");
            int volume = Int32.Parse(Console.ReadLine());
            string str = date + ';' + eventDescription + ';';
            if (volume == 0)
                Console.WriteLine("Людей, участвовших в этом событии, нет.");
            else
            {
                while (volume > 0)
                {
                    Person id = FindPeopleLogic.FindPeople(people);
                    volume--;
                    str = str + ';' + id.id + ';';
                }
            }
            if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                File.AppendAllText(path, str + "\n");
            else
                JSON.WriteTimelineJSON(timeline, path);
        }

        static int GetId(List<Person> people)
        {
            int maxId = 0;
            string str = File.ReadAllText(@"..\..\..\..\people.csv");
            if (str == "\r\n")
            {
                maxId = 1;
            }
            else
            {
                foreach (Person person in people)
                {
                    maxId = person.id;
                }
                maxId++;
            }
            return maxId;
        }

        public static List<Person> ExecuteAddPeople(List<Person> people)
        {
            string path = WorkingWithTheExtension.Start("people");
            Person person = new Person();
            Console.WriteLine("Введите имя: ");
            string name = Console.ReadLine();
            person.name = name;
            string birthDay = NewBirthOrDeathDay.ReadBirthday(name);
            person.birthday = ParseDateTime.ParseDate(birthDay);
            string deadDay = NewBirthOrDeathDay.ReadDeathDay(name, birthDay);
            person.deathday = ParseDateTime.ParseDate(deadDay);
            string str = "";
            if (deadDay != null)
                str = Convert.ToString(GetId(people)) + ';' + name + ';' + birthDay + ';' + deadDay;
            else
                str = Convert.ToString(GetId(people)) + ';' + name + ';' + birthDay;

            Console.WriteLine("Нажмите Enter, если у человека неизвестны родители, или кнопку F, чтобы найти родственников: ");
            ConsoleKeyInfo keyInfo = Console.ReadKey(true);
            while (keyInfo.Key != ConsoleKey.Enter && keyInfo.Key != ConsoleKey.F)
            {
                Console.WriteLine("Ошибка! Нажмите Enter, если у человека неизвестны родители, или кнопку F, чтобы найти родственников: ");
                keyInfo = Console.ReadKey(true);
            }
            Person parent1; Person parent2;
            if (keyInfo.Key == ConsoleKey.F)
            {
                parent1 = FindPeopleLogic.FindPeople(people);
                str = str + ';' + Convert.ToString(parent1.id);
                person.parentId1 = parent1.id;
                Console.WriteLine("Есть ли у человека еще один родитель?(Enter - нет, F - да)");
                keyInfo = Console.ReadKey(true);
                while (keyInfo.Key != ConsoleKey.Enter && keyInfo.Key != ConsoleKey.F)
                {
                    Console.WriteLine("Ошибка! Нажмите Enter, если у человека неизвестны родители, или кнопку F, чтобы найти родственников: ");
                    keyInfo = Console.ReadKey(true);
                }
                if (keyInfo.Key == ConsoleKey.F)
                {
                    parent2 = FindPeopleLogic.FindPeople(people);
                    str = str + ';' + Convert.ToString(parent2.id);
                    person.parentId2 = parent2.id;
                }
            }
            str += ';';
            people.Add(person);
            if (path.Length - 3 == 'c' || path.Length - 3 == 'C')
                File.AppendAllText(path, str);
            else
                JSON.WritePeopleJSON(people, path);
            return (people);
        }
    }
}
