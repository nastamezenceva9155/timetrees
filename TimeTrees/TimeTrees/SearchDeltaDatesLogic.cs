﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class SearchDeltaDatesLogic
    {
        static (DateTime, DateTime) MinAndMax(List<TimelineEvent> timeLine)
        {
            DateTime min = DateTime.MaxValue, max = DateTime.MinValue;

            for (var i = 0; i < timeLine.Count; i++)
            {
                var date = timeLine[i].date;
                if (date < min) min = date;
                if (date > max) max = date;
            }
            return (max, min);
        }

        static (int, int, int) DeltaMinAndMaxDate(List<TimelineEvent> timeLine)
        {
            (DateTime max, DateTime min) = MinAndMax(timeLine);

            return (Math.Abs(max.Year - min.Year), Math.Abs(max.Month - min.Month), Math.Abs(max.Day - min.Day));
        }

        public static void ExecuteDeltaProgram(List<TimelineEvent> timeLine2)
        {
            (int year, int month, int day) = DeltaMinAndMaxDate(timeLine2);
            Console.WriteLine($"Прошло {year} лет {month} месяцев {day} дней");
        }
    }
}
