﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class TransferFromArrayToStruct
    {
        public static List<TimelineEvent> TimeLineToEvent(string[][] timeLine)
        {
            List<TimelineEvent> events = new List<TimelineEvent>();
            for (var i = 0; i < timeLine.Length; i++)
            {
                TimelineEvent timeline = new TimelineEvent();
                timeline.date = ParseDateTime.ParseDate(timeLine[i][TimelineEvent.TimelineDateIndex]);
                timeline.description = timeLine[i][TimelineEvent.TimelineDescriptionIndex];
                if (timeLine[i].Length == 3)
                    timeline.id += timeLine[i][2];
                else
                    for (int j = 2; j < timeLine[i].Length; j++)
                        timeline.id = timeline.id + timeLine[i][j] + "; ";
                events.Add(timeline);
            }

            return events;

        }

        public static List<Person> PeopleToPerson(string[][] people)
        {
            List<Person> arr = new List<Person>();
            for (int i = 0; i < people.Length; i++)
            {
                Person person = new Person();
                person.name = people[i][Person.NameIndex];
                person.id = int.Parse(people[i][Person.IdIndex]);
                person.birthday = ParseDateTime.ParseDate(people[i][Person.BirthIndex]);
                if (people[i].Length > 3)
                    if (people[i][Person.DeathIndex] != "")
                        person.deathday = ParseDateTime.ParseDate(people[i][Person.DeathIndex]);
                if (people[i].Length > 4)
                {
                    if (people[i][Person.ParentID1] != "")
                        person.parentId1 = int.Parse(people[i][Person.ParentID1]);
                    if (people[i].Length > 5)
                        if (people[i][Person.ParentID2] != "")
                            person.parentId2 = int.Parse(people[i][Person.ParentID2]);
                }
                arr.Add(person);
            } 
            return arr;
        }

    }
}
