﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TimeTrees
{
    class LeapYearLogic
    {
        /*static List<Person> GetPeople(List<Person> people)
        {
            List<Person> listPeople = new List<Person>();
            for (var j = 0; j < people.Count; j++)
            {
                listPeople.Add(people[j]);
            }
            return listPeople;
        }*/

        public static void ExecuteLeapYearProgram(List<Person> people)
        {
            for (int i = 0; i < people.Count; i++)
            {
                var date = people[i].birthday;
                DateTime today = DateTime.Today;
                if ((DateTime.IsLeapYear(date.Year)))
                {
                    //if (today.Year - date.Year < 21 || people[i].deathday.Year - date.Year < 21)
                    Console.WriteLine(people[i].name);
                }
            }
        }
    }
}
