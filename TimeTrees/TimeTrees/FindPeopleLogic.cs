﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class FindPeopleLogic
    {
        private static List<Person> FilterPeople(List<Person> people, string name)
        {
            List<Person> result = new List<Person>();
            foreach (Person person in people)
            {
                if (person.name.Contains(name, StringComparison.OrdinalIgnoreCase))
                    result.Add(person);
            }
            return result;
        }

        static void PrintPeople(List<Person> people, int? selectedIndex)
        {
            for (var i = 0; i < people.Count; i++)
            {
                Person person = people[i];
                if (selectedIndex == i)
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(person.name);
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        public static Person FindPeople(List<Person> people)
        {
            
            List<Person> found = new List<Person>();
            string name = null;
            int? selectedId = null;
            Person? selectedPerson = null;

            do
            {
                ConsoleHelper.ClearScreen();
                string hint = "Начните вводить имя: ";
                Console.WriteLine("ПОИСК ЛЮДЕЙ");
                Console.CursorVisible = true;
                Console.WriteLine($"{hint}{name}");
                if (string.IsNullOrEmpty(name))
                    found = people;
                else
                    found = FilterPeople(people, name);
                PrintPeople(found, selectedId);
                Console.SetCursorPosition($"{hint}{name}".Length, 1);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (Char.IsLetter(keyInfo.KeyChar))
                {
                    name += keyInfo.KeyChar;
                    selectedId = null;
                }
                else if (keyInfo.Key == ConsoleKey.Backspace)
                    name = name.Remove(name.Length - 1);
                else if (keyInfo.Key == ConsoleKey.DownArrow)
                    if (!selectedId.HasValue)
                        selectedId = 0;
                    else if (selectedId + 1 < found.Count)
                        selectedId++;
                    else
                        selectedId = 0;
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                    if (!selectedId.HasValue)
                        selectedId = 0;
                    else if (selectedId - 1 < 0)
                        selectedId = found.Count - 1;
                    else
                        selectedId--;
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    if (selectedId.HasValue)
                    {
                        selectedPerson = found[selectedId.Value];
                        break;
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    break;
                }
                    

            } while (true);
            ConsoleHelper.ClearScreen();
            return (Person)selectedPerson;
        }
    }
}
