﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class TimelineEvent
    {
        public const int TimelineDateIndex = 0;
        public const int TimelineDescriptionIndex = 1;
        public string description;
        public DateTime date;
        public string id = "";
        public void DisplayInfo()
        {
            Console.Write($"Description: {description}    Date: {date}    ");
            if (id != "")
                Console.WriteLine($"Participants: {id}");
        }
    }
}
