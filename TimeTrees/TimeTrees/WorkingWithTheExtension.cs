﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees
{
    class WorkingWithTheExtension
    {
        static string Expansion(string expansion, string file)
        {
            string path;
            if (file == "people")
            {
                if (expansion == "CSV" || expansion == "csv")
                    path = @"..\..\..\..\people.csv";
                else
                    path = @"..\..\..\..\people.json";
            }
            else
            {
                if (expansion == "CSV" || expansion == "csv")
                    path = @"..\..\..\..\timeline.csv";
                else
                    path = @"..\..\..\..\timeline.json";
            }
            return path;
        }

        public static string Start(string file)
        {
            Console.WriteLine("С каким файлом хотите работать?(CSV/JSON)");
            string expansion = Console.ReadLine();
            while (!CheckingForCorrectness.ExpansionCorrectness(expansion))
            {
                Console.WriteLine("Ошибка! Введите корректное расширение!(CSV/JSON)");
                expansion = Console.ReadLine();
            }
            string path = Expansion(expansion, file);
            ConsoleHelper.ClearScreen();
            return path;
        }
    }
}
